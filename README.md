# Enchanced ChatGPT

## Getting started

**Running the UI**
- Change Directory to `\client`: `$ cd client`
- Run: `$ npm start`
    - Visit `localhost:3000\` to see your application running

**Serving the Backend**
- Run: `$ node index.js`


## Name
Project Name: Enchanced ChatGPT

## Description
This project is my version of ChatGPT. On the front-end we have a simple chatting interface (very similar to the original ChatGPT) powered by OpenAI API's.
You can choose your AI engine model to get the best possible answers to your questions. Also, choose how creative or logical you want your answers to be, and 
long they should be!

**Features:**
- Create new chats
- Clear existing chats
- Choose your AI engines
- Choose the randomness of your model
- Choose the number of tokens

## Badges
No Test Cases.

## Visuals
### UI Interface
![UI Interdace](assets/ui-interface.png)
![Chat Interdace](assets/chat-interface.png)

## Installation
1. Clone the project from Gitlab/Github
2. Do: `$ cd enchances-chatgpt` to change directory into the project
3. Run: `$ npm install express cors body-parser openai` to install dependencies
4. Run: `$ node index.js` to start the server
4. Do: `$ cd client` to change directory into the client project folder
3. Run: `$ npm install` to install dependencies
4. Run: `$ npm start` to start the server for hosting the web application in React

## Usage
Enter an input prompt, and except a randomized answer to your question or prompt provided by ChatGPT

## Support
For support, contact: `2002sahapriya@gmail.com`

## Roadmap
Not released yet.


## Authors and acknowledgment
Author: Priyadarshini Saha

**Resources:**
- Youtube: https://www.youtube.com/watch?v=qwM23_kF4v4
- [OpenAI](https://www.openai.com)

## License
None

## Project status
- Will add more features in the future