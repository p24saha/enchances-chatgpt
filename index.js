// 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

// Authentication
const { Configuration, OpenAIApi } = require("openai");
const express = require('express')

// add body parser and corrs to express
const bodyParser = require('body-parser');
const cors = require('cors')

const configuration = new Configuration({
    organization: "org-sf2Sa4tn62v7vKDvDziMJKEf",
    // apiKey: process.env.OPENAI_API_KEY,
    apiKey: "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",
});
const openai = new OpenAIApi(configuration);



// Create a simple express API - that calls the fuction above
const app = express()
app.use(bodyParser.json())
app.use(cors())

const port = 3080

app.post('/', async (req, res) => {
    const { message, currentModel, currentTemperature, currentToken } = req.body;
    console.log(currentToken, currentModel, currentTemperature)
    const response = await openai.createCompletion({
        model: `${currentModel}`,
        prompt: `${message}`,
        max_tokens: currentToken,
        temperature: currentTemperature
    });

    res.json({ 
        message: response.data.choices[0].text,
    })
});

app.get('/models', async (req, res) => {
    const response = await openai.listEngines();
    res.json({
        models: response.data.data
    })
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});