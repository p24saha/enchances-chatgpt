import './App.css';
import './normal.css'
// import setState
import { useState, useEffect} from 'react'


function App() {
  // use effect run once when app loads
  useEffect(() => {
    getEngines();
  }, [])

  // add state for input and chat log
  const [input, setInput] = useState('');
  const [currentModel, setCurrentModel] = useState("ada");
  const [temperature, setTemperature] = useState(0.5);
  const [token, setToken] = useState(100);
  const [models, setModels] = useState([]);
  const [chatLog, setChatLog] = useState([
    // { 
    //   user: "gpt",
    //   message: "How can I help you today?"
    // },
    // {
    //   user: "Priya",
    //   message: "I want to use ChatGPT today"
    // }
  ]);


  function getEngines() {
    fetch( "http://localhost:3080/models")
    .then(res => res.json())
    .then(data => setModels(data.models))
  }

  // clear chat
  function clearChat() {
    setChatLog([])
  }
  // Clear and reset chatlogs
  function newChat() {
    setChatLog([])
    setTemperature(0.5)
    setToken(100)
  }
  
  // submit function
  async function handleSubmit(e) {
    e.preventDefault();
    let chatLogNew = [...chatLog, { user: "Priya", message: `${input}`}]
    await setInput("")
    setChatLog(chatLogNew)

    const messages = chatLogNew.map((data) => data.message).join("\n")
    let currentTemperature = Number(temperature)
    let currentToken = Number(token)

    if (currentTemperature < 0 || currentTemperature > 1) {
      setTemperature(0.5);
      currentTemperature = Number(temperature)
    }

    if (currentToken < 16 || currentToken > 4096) {
      setToken(16)
      currentToken = Number(token)
    }

    // fetch request to the api combining the chat log array of messages and sending it as a message to locahost:3000 as a post
    const response = await fetch("http://localhost:3080/", {
      method: "POST",
      headers: { 
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "http://localhost:3080"
      },
      body: JSON.stringify({
        message: messages,
        currentModel,
        currentTemperature,
        currentToken,
      })
    })

    const data = await response.json();
    setChatLog([...chatLogNew, {user: "gpt", message: `${data.message}`}])
  }


  return (
    <div className="App">
       {/* Aside menu */}
       <div className="sidemenu">
        <div className="side-menu-button" onClick={newChat}>
          <span>+</span>
          New Chat
        </div>
          {/* Models */}
          <div className="models">
            <div className="models-label">Models</div>
            <select className="models-dropdown" onChange={(e) => setCurrentModel(e.target.value)}>
              {models.map((model) => 
                <option key={model.id} value={model.id}>{model.id}</option>
              )}
            </select>
            {/* Models Information */}
            <div className="models-info">
              <div className="models-info-labels">Smart - Davinci</div>
              <div className="models-info-labels">Code - Crushman</div>
              <div className="models-text">
                <span>The model parameter controls the engine used to generate the response. Davinci produces the best results.</span>
              </div>
            </div>
          </div>

          {/* Temperature */}
          <div className="temperature">
            <div className="temperature-label">Temperature</div>
            <form>
              <input
                className="temperature-input"
                rows="1"
                value={temperature}
                onChange={(e) => setTemperature(e.target.value)}
              ></input>
            </form>
            {/* Temperature Information */}
            <div className="temperature-info">
              <div className="temperature-info-labels"> 0 - Logical</div>
              <div className="temperature-info-labels"> 0.5 - Balanced</div>
              <div className="temperature-info-labels"> 1 - Creative</div>

              <div className="temperature-text">
                <span>The temperature controls the randomness of the model. 0 is the most logical, 1 is the most creative.</span>
              </div>
            </div>
          </div>

          {/* Max Tokens */}
          <div className="max-tokens">
            <div className="token-label">Max Tokens</div>
            <form>
              <input
                className="token-input"
                rows="1"
                value={token}
                onChange={(e) => setToken(e.target.value)}
              ></input>
            </form>
            {/* Token Information */}
            <div className="temperature-info">
              <div className="token-info-labels"> 16 - Default</div>
              <div className="token-info-labels"> 2048 - Most models</div>
              <div className="token-info-labels"> 4096 - New Models</div>

              <div className="token-text">
                <span>The maximum number of tokens to generate in the completion.</span>
              </div>
            </div>
          </div>

          {/* Clear Button */}
          <div className="clear-chat" onClick={clearChat}>
                Clear Chat
          </div>   
       </div>

       {/* Main Section */}
       <section className="chatbox">
        {/* Chat Log */}
        <div className="chat-log">
           {/* User Message View */}
           {chatLog.map((data, index) => (
            <ChatMessage key={index} message={data.message} user={data.user} />
           ))}
          

           {/* ChatGpt Message View */}
          
        </div>

        {/* Chat Textarea */}
        <div className="chat-input-holder">
          <form onSubmit={handleSubmit}>
            <input 
              rows="1" 
              value={input} 
              onChange={(e) => setInput(e.target.value)}
              className="chat-input-textarea" 
              placeholder="Ask me anything!">
            </input>
          </form>
          
        </div>
       </section>
    </div>
  );
}


const ChatMessage = ({ message, user }) => {
  return (
    <div className={`chat-message ${user === "gpt" && "chatgpt"}`}>
      <div className="chat-message-center">
        {/* User avatar */}
        <div className={`avatar ${user === "gpt" && "chatgpt"}`}>
          {user === "gpt" && 
            <svg
            width={41}
            height={41}
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            strokeWidth={1.5}
            className="h-6 w-6"
          >
            <path
              d="M37.532 16.87a9.963 9.963 0 0 0-.856-8.184 10.078 10.078 0 0 0-10.855-4.835A9.964 9.964 0 0 0 18.306.5a10.079 10.079 0 0 0-9.614 6.977 9.967 9.967 0 0 0-6.664 4.834 10.08 10.08 0 0 0 1.24 11.817 9.965 9.965 0 0 0 .856 8.185 10.079 10.079 0 0 0 10.855 4.835 9.965 9.965 0 0 0 7.516 3.35 10.078 10.078 0 0 0 9.617-6.981 9.967 9.967 0 0 0 6.663-4.834 10.079 10.079 0 0 0-1.243-11.813ZM22.498 37.886a7.474 7.474 0 0 1-4.799-1.735c.061-.033.168-.091.237-.134l7.964-4.6a1.294 1.294 0 0 0 .655-1.134V19.054l3.366 1.944a.12.12 0 0 1 .066.092v9.299a7.505 7.505 0 0 1-7.49 7.496ZM6.392 31.006a7.471 7.471 0 0 1-.894-5.023c.06.036.162.099.237.141l7.964 4.6a1.297 1.297 0 0 0 1.308 0l9.724-5.614v3.888a.12.12 0 0 1-.048.103l-8.051 4.649a7.504 7.504 0 0 1-10.24-2.744ZM4.297 13.62A7.469 7.469 0 0 1 8.2 10.333c0 .068-.004.19-.004.274v9.201a1.294 1.294 0 0 0 .654 1.132l9.723 5.614-3.366 1.944a.12.12 0 0 1-.114.01L7.04 23.856a7.504 7.504 0 0 1-2.743-10.237Zm27.658 6.437-9.724-5.615 3.367-1.943a.121.121 0 0 1 .113-.01l8.052 4.648a7.498 7.498 0 0 1-1.158 13.528v-9.476a1.293 1.293 0 0 0-.65-1.132Zm3.35-5.043c-.059-.037-.162-.099-.236-.141l-7.965-4.6a1.298 1.298 0 0 0-1.308 0l-9.723 5.614v-3.888a.12.12 0 0 1 .048-.103l8.05-4.645a7.497 7.497 0 0 1 11.135 7.763Zm-21.063 6.929-3.367-1.944a.12.12 0 0 1-.065-.092v-9.299a7.497 7.497 0 0 1 12.293-5.756 6.94 6.94 0 0 0-.236.134l-7.965 4.6a1.294 1.294 0 0 0-.654 1.132l-.006 11.225Zm1.829-3.943 4.33-2.501 4.332 2.5v5l-4.331 2.5-4.331-2.5V18Z"
              fill="currentColor"
            />
          </svg>
          }
          {user !== "gpt" &&
            <svg className="bomb-party" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 500">
            <path d="M459.1 52.4 442.6 6.5c-1.9-3.9-6.1-6.5-10.5-6.5s-8.5 2.6-10.4 6.5l-16.5 45.9-46 16.8c-4.3 1.6-7.3 5.9-7.2 10.4 0 4.5 3 8.7 7.2 10.2l45.7 16.8 16.8 45.8c1.5 4.4 5.8 7.5 10.4 7.5s8.9-3.1 10.4-7.5l16.5-45.8 45.7-16.8c4.2-1.5 7.2-5.7 7.2-10.2 0-4.6-3-8.9-7.2-10.4l-45.6-16.8zm-132.4 53c-12.5-12.5-32.8-12.5-45.3 0l-2.9 2.9c-22-8-45.8-12.3-70.5-12.3C93.1 96 0 189.1 0 304s93.1 208 208 208 208-93.1 208-208c0-24.7-4.3-48.5-12.2-70.5l2.9-2.9c12.5-12.5 12.5-32.8 0-45.3l-80-80zM200 192c-57.4 0-104 46.6-104 104v8c0 8.8-7.2 16-16 16s-16-7.2-16-16v-8c0-75.1 60.9-136 136-136h8c8.8 0 16 7.2 16 16s-7.2 16-16 16h-8z" />
          </svg>
          }
        </div>

        {/* User generic message */}
        <div className="message">
          {message}
        </div>
      </div>
    </div>
  )
}

export default App;

